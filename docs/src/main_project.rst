****************
Master's project
****************

This project is a part of the education at the `University of Agder <https://www.uia.no/studier/mekatronikk-sivilingenioer>`_ 
as the final project for a Master of Science in Mechatronics. The project build upon results from the conducted :ref:`Pre-study project`.


IR camera
=========
The thermal imaging camera implemented for testing in this project is the `Optris PI 400i <https://www.optris.global/thermal-imager-optris-pi-400i-pi-450i>`_.






ROS package
===========
Robot Operating System (ROS) is a framwork designed for writing software, that can be implemented to a wide range of platforms. ROS is a robust plattform for handling different 
nodes of software written in different languages, allowing for cross-execution and communication between nodes.  

After installing `ROS Melodic Morenia <http://wiki.ros.org/melodic>`_ and the `Optris IR Imager Direct SDK <http://documentation.evocortex.com/libirimager2/html/index.html>`_  
for Ubuntu 18.04. The ROS package for `Optris thermal imager devices <http://wiki.ros.org/optris_drivers>`_ can be downloaded, this package contains nodes for
reading thermal raw data from the camera, converting the raw data to visible images and calculating the temperature matrix. 



Setup
-----









