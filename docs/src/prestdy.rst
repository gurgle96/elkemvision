*****************
Pre-study project
*****************

Commonly used resources
=======================
`OpenCV-Python Tutorials <https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_tutorials.html>`_

`Sphinx/Rest <https://rest-sphinx-memo.readthedocs.io/en/latest/#>`_

`UiA MAS-507 <https://uia-mekatronikk.gitlab.io/mas507/index.html>`_

`GitLab UiA MAS-507 <https://gitlab.com/uia-mekatronikk/mas507>`_ 


Detection with OpenCV
=====================
OpenCV is a library for computer vision and machine learning tools, open-source and contributes to 
a common platform within the science of computer vision. The library consists of more than 2500 optimized
algorithms, counting on both standard/classic computer vision tasks and modern/state-of-the-art solutions 
for vision and machine learning. 

.. pull-quote:: 

    *These algorithms can be used to detect and recognize faces, identify objects, classify human actions 
    in videos, track camera movements, track moving objects, extract 3D models of objects, produce 3D point 
    clouds from stereo cameras, stitch images together to produce a high resolution image of an entire scene, 
    find similar images from an image database, remove red eyes from images taken using flash, follow eye movements, 
    recognize scenery and establish markers to overlay it with augmented reality, etc.*


`OpenCV <https://opencv.org/about/>`_

Thresholding method
-------------------

The detection and segmentation of the liquid metal tapping beam is handeled in the function :code:`TappingBeam.py`. 
Input for this function is a RGB-image of the tapping beam, this is then filtered with a gaussian blur, to smoothen the image.
Then converted to HSV (Hue, Saturation and Value) for thresholding the picture for the range of white defined with.

:code:`white_lower = np.array([0,0,245])`
:code:`white_upper = np.array([255,255,255])`

The left image is the original RGB image of a tapping beam and the image to the left is a HSV image.

.. image:: ../figs/original_tappingbeam.png
    :width: 8.5cm
    :alt: 
    :align: left  

.. image:: ../figs/hsv_tappingbeam.png
    :width: 8.5cm
    :alt: 
    :align: right  


A mask is created with the :code:`cv2.inRange()`, a function that can detect objects based on their HSV values. 
A kernel is constructed with the function :code:`kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))`. 
:code:`image_closed = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)` closes small specks of holes inside the foreground figure.
:code:`image_closed_then_opened = cv2.morphologyEx(image_closed, cv2.MORPH_OPEN, kernel)`


:code:`image_closed_then_opened` is a binary image of the detected tapping beam, next task is to find the coordinates for 
every pixel that has a value of 255. A new image with the same size as the input image is then created, this image is an 
all black background for the segmentation of tappingbeam. The origian image is used and only the coordinates where there 
exist a tapping beam is used to transfer the RGB image pixels over to the black image. 


.. image:: ../figs/segmented_tappingbeam.png
    :width: 8.5cm
   

Code
^^^^

.. code-block:: 

    def Threshold(image_path):

        # Read image, 0 integer for grayscale
        image_original= cv2.imread(image_path,0)

        # Convert from BGR to RGB
        image_RGB = cv2.cvtColor(image_original, cv2.COLOR_BGR2RGB)

        # Blur image slightly
        image_RGB = cv2.GaussianBlur(image_RGB, (5, 5), 0)

        # Convert to HSV
        image_HSV = cv2.cvtColor(image_RGB, cv2.COLOR_RGB2HSV)

        # Lower range for white
        white_lower = np.array([0,0,245])

        # Upper range for white
        white_upper = np.array([0,0,255])

        # Threshold HSV image to get only white colours
        mask = cv2.inRange(image_HSV, white_lower, white_upper)

        # Fill small gaps and remove specks
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        image_closed = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        image_closed_then_opened = cv2.morphologyEx(image_closed, cv2.MORPH_OPEN, kernel)

        # Finds the coordinates where the pixel value is 255
        indices = np.where(image_closed_then_opened == 255)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        # Create new image from the original image
        image_black = image_original

        # Convert to black image
        image_black = np.ones(image_black.shape)

        # Isolate the tapping beam ROI from the original image
        tapping_beam = image_original[coordinates_x,coordinates_y]

        # Add the isolated ROI to the black image
        image_black[coordinates_x,coordinates_y] = tapping_beam

        #cv2.imwrite('out.png',image_black)

        return(image_black)


Histogram Equalization method
-----------------------------

Histogram equalization, is a method of increasing the global contrast in an image based upon its own histogram.
This is accomplished by effecticely spreading out the most frequent intensity values. 
Allowing areas of lower contrast to gain a higher contrast. 


.. image:: ../figs/histeq.jpg
    :align: center

|

.. image:: ../figs/unequalized_image.jpg
    :width: 8.5cm
    :align: left

.. image:: ../figs/equalized_image.jpg
    :width: 8.5cm
    :align: right

|
|
|
|
|
|
|
|
|
|



Code
^^^^

.. code-block::

    def Threshold(image_path):

        # Read image, 0 integer for grayscale
        image_original= cv2.imread(image_path,0)

        # Convert from BGR to RGB
        image_RGB = cv2.cvtColor(image_original, cv2.COLOR_BGR2RGB)

        # Blur image slightly
        image_RGB = cv2.GaussianBlur(image_RGB, (5, 5), 0)

        # Convert to HSV
        image_HSV = cv2.cvtColor(image_RGB, cv2.COLOR_RGB2HSV)

        # Lower range for white
        white_lower = np.array([0,0,245])

        # Upper range for white
        white_upper = np.array([0,0,255])

        # Threshold HSV image to get only white colours
        mask = cv2.inRange(image_HSV, white_lower, white_upper)

        # Fill small gaps and remove specks
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        image_closed = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        image_closed_then_opened = cv2.morphologyEx(image_closed, cv2.MORPH_OPEN, kernel)

        # Finds the coordinates where the pixel value is 255
        indices = np.where(image_closed_then_opened == 255)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        # Create new image from the original image
        image_black = image_original

        # Convert to black image
        image_black = np.ones(image_black.shape)

        # Isolate the tapping beam ROI from the original image
        tapping_beam = image_original[coordinates_x,coordinates_y]

        # Add the isolated ROI to the black image
        image_black[coordinates_x,coordinates_y] = tapping_beam

        #cv2.imwrite('out.png',image_black)

        return(image_black)


Convolutional Neural Network
============================

As a part of the pre-study, a course from `NVIDIA Deep Learning Institute <https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/about>`_
was conducted in order to gain experience with AI on the Jetson Nano. The course gave an introduction to the use of Convolutional Neural Network (CNN), 
ResNet-18, labeling images for creating a dataset and transfer learning using Python on a Linux platform. Resulting in trained residual networks for 
categorizing and finding the coordinates of features in images. The source-code used in this course will be 
applied in the following master thesis project and further developed. 


Flask web HMI
======================

In order to create a user-friendly interface/HMI, a web application was chosen due to the cross-platform compatebility. 
A responsive website with the ability to re-shape the layout depening on screen size and resolution increases the availbilty of information and process overview. 
Allowing production parameters to be monitored and altered from a smartphone, tablet or pc.


.. image:: ../figs/web_based_HMI.jpg
    :width: 10cm
    :align: center

The application for this project is developed using Flask, described in :ref:`Installations`. Flask offers a simple layer to attach more advanced Python extensions and features to.
Templates for layout are created with :code:`HTML`, a base template that configures the overall layout is extended troughout every sub-page. 
`Bootstrap <https://getbootstrap.com/docs/5.0/getting-started/introduction/>`_ provides a library of functions in :code:`HTML`, :code:`CSS` and :code:`JS` for creating a
dynamic and responsive front-end.  

`Example of responsive layout with Bootstrap <https://getbootstrap.com/docs/5.0/examples/dashboard/>`_



Application structure
---------------------
The flask application structure is as follows: 


.. code-block::

    ├───src
    │       MainProgram.py
    │
    └───webapp
        │   __init__.py
        │
        ├───static
        │       elkem-logo.png
        │       Ovn2_temperature.svg
        │
        ├───templates
        │       base.html
        │       home.html
        │       parameters.html
        │       tapping.html
        │
        └───views
                home.py
                parameters.py
                tapping.py
                __init__.py


Executing :code:`MainProgram.py` builds the website, and runs the tapping beam detection algorithms. 
Publishing images and process parameters to the website HMI. Note that the website is only hosted on the local network between the Jetson Nano and the connected computer. 

.. image:: ../figs/Flask_home_page.png
    :width: 8.5cm
    :align: left

.. image:: ../figs/Flask_tapping.png
    :width: 8.5cm
    :align: right