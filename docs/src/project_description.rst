Project Description
===================
Pre-study project for Mechatronics MAS500 master thesis at the University of Agder, done on behalf of Elkem Technology through the consulatncy firm Young Industrial Innovators (YI2). 
The intent of this project is to build tools for image processing, gather requirements and solve inital challenges regarding flow and temperature estimation of tapping beam from furnace.
IR camera is used as the sensor and the detection is based upon OpenCV.

.. figure:: ../figs/tapping_beam.png
   :width: 15cm

**Research questions:**

* Detection of tapping beam.

* Measuring metal temperature in tapping beam.

* Measuring the flow of metal in tapping beam.

**Focus areas for pre-study project:**

* Build basic applications for detection of tapping beam from image.

* Build algorithm for calculating the tapping beam temperature from IR camera image.

* Implement Flask webpage.

* Study solutions for machine learning.
