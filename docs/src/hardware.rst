**************************
Hardware and Installations
**************************

Overview of hardware used in the project and guides for installing the required software.

NVIDIA Jetson Nano
==================

.. image:: ../figs/jetsonnano.png
    :width: 8.5cm
    :alt: 
    :align: right  

The NVIDIA Jetson Nano is small, powerfull single-board computer pruposfully designed for embedded AI applications, image
classifaction and segmentation, object detection and speech processing. All incorporated into a small efficient 
package. Ubuntu 18.04 is the base operating system, with NVIDIA JETPACK running upon.

.. pull-quote:: 
    *NVIDIA JetPack SDK is the most comprehensive solution for building AI applications. It
    includes the latest OS images for Jetson products, along with libraries and APIs,
    samples, developer tools, and documentation.*


.. image:: ../figs/nvidia_logo.svg
    :width: 400cm
    :alt: 
    :align: center

**Technical Specifications**


* `NVIDIA Developer: <https://developer.nvidia.com/embedded/jetson-nano-developer-kit>`_

Installations
-------------

**Operating System**

The OS for the Nano is downloaded directly from NVIDIA through the link below, and the installations process is 
comprehensively documented through the setup guide. The setup in this projects was done with a screen and 
keyboard/mouse connected and a 64 GB SD card.

`Jetson Nano SDK image <https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit>`_


**SSH**

The programming and use of the Jetson Nano is done from a host computer via Secure Shell (SSH) and 
Microsoft Visual Studio Code (VS Code) is used as the source code editor. VS Code is free to download for Windows, Mac and Ubuntu/Linux.
It also features a rich ecosystem of extensions for other languages (such as C++, C#, Java, **Python**, PHP, Go) and 
runtimes (such as .NET and Unity). 

* `VS Code <https://code.visualstudio.com/>`_

* `Internet via Ethernet <https://datawookie.netlify.app/blog/2018/10/dns-on-ubuntu-18.04/>`_




**OpenCV**

.. image:: ../figs/openCV_logo.svg
    :width: 150cm
    :align: center



OpenCV must be installed on the Jetson Nano in order to use CUDA accelerated processing, the Jetson Nano Image that is already installed 
inludes a version of OpenCV with no CUDA support. Note that Qt5 was purposly not installed in this project.

* `OpenCV 4.5 <https://qengineering.eu/install-opencv-4.5-on-jetson-nano.html?fbclid=IwAR2nBLkmc-eP5xr6KD3PM5JiYhWp88W2h33L0rHMltelHgso0G32RumYWWs>`_


**Python and NumPy**

.. image:: ../figs/python_logo.svg
    :width: 100cm
    :align: left

.. image:: ../figs/numpy_logo.svg
    :width: 200cm
    :align: center 

|


Python 3 and NumPy comes with the installing of OpenCV. The installed version of python can be checked by the running this command :code:`python --version` in 
the Jetson Nano terminal window. 

* `Python 3 <https://www.python.org/download/releases/3.0/>`_


**Flask**

Flask is Python web development tool that is used for building web applications, supporting a large number of extensions and 
many hooks to customize it's behavior. 
Flask is installed on the Jetson Nano through the following command in the terminal window, :code:`pip3 install Flask`.


.. pull-quote:: 

    *Flask is a small and lightweight Python web framework that provides useful tools and features that make creating web applications in Python easier. 
    It gives developers flexibility and is a more accessible framework for new developers since you can build a web application quickly using only a single Python file. 
    Flask is also extensible and doesn’t force a particular directory structure or require complicated boilerplate code before getting started.*


**IR Imager Direct SDK**

`IRImagerDirect SDK <https://evocortex.org/products/irimagerdirect-sdk/>`_, is the supplied software development kit (SDK) for the 
Optris PI thermal imaging cameras. Available as both Debian and Windows package. Based upon a C-style interface and a object oriented C++ interface 
for data acquisition. Supporting architechtures such x64,x86 and ARM. Read the `documentation <http://documentation.evocortex.com/libirimager2/html/Installation.html>`_ 
for further information about installation and how-to's.









Configuring SSH and Visual Studio Code
--------------------------------------
The Linux operating system on the Jetson can be used and operated using only the terminal (text input and output).
You can update system files, edit files directly in terminal, and run code, all from the mighty terminal in Linux.

SSH is an abbreviation for "Secure SHell" and allows for opening such a terminal session on a remote host.
This means you can use a Windows computer to operate a Linux system using text input.

Microsoft Visual Studio Code (VSCode) is a general source-code editor with the ability to edit code on remote Linux servers over an SSH connection.
This editor is fast, lightweight and has good color-coding for Python code. 
You can also open SSH terminals inside VSCode. 


**Enable OpenSSH Client Feature in Windows 10**

Windows 10 does not have good SSH capabilities by default.
To enable some recommended features, an optional OpenSSH client needs to be installed in Windows 10:

* Be logged in as an administrator.
* Navigate to Windows settings -> Apps.

  * Click :code:`Optional Features` at the top of the list.
  * Add optional features.
  * Find :code:`OpenSSH Client` and install.

* Click [WIN + R] to open run command.

  * :code:`services.msc`.
  * Find :code:`OpenSSH Authentication Agent` in the list.

    * Open properties and set it to automatically start.
    * Click :code:`apply` and :code:`start`.

.. image:: ../figs/openssh_client.png
    :width: 15cm

**Configure Visual Studio Code** 

`Download <https://aka.ms/win32-x64-user-stable>`_ and install VSCode. 
Default installation is alright. 

Open VSCode and Install Remote SSH extension
- Click extensions button on the left side
- Search for and install `Remote - SSH`.
- Restart VSCode if prompted to do so.

- Click the green icon in the lower-left corner of VSCode to enter remote session.

  - Choose `Remote-SSH: Connect to host`.
  - Add a new SSH host.
  - Type in :code:`username@hostname` or :code:`username@ip-adress`.

User name and host name can be found by connecting a screen to the Jetson Nano and open a terminal, 
:code:`username@hostname` can then be seen.

- Choose to edit the configuration file under :code:`C:\Users\%USERNAME%\.ssh\config`
- Configre the file so that is follows this form: 

.. image:: ../figs/ssh_config.png
    :width: 5cm
    :align: center


Sharing Internet over ethernet
------------------------------

The Jetson will get internet from a Windows computer by sharing over Ethernet cable, WiFi is also possible. 

* Connect the Jetson Nano to a PC with a ethernet cable.
* Open :code:`Network & Internet` in :code:`Settings`.
* Open :code:`Ethernet` and press :code:`Change adapter options`.
* The ethernet connection should now be visible:

.. image:: ../figs/network_connections.png
    :width: 20cm

* Right click :code:`Wi-Fi` and select :code:`Properties`.

* In the :code:`Sharing` header, tick the box that says: :code:`Allow other network users to connect through this computer's Internet connection`.

.. image:: ../figs/wifi_properties.png
    :width: 7cm
    :align: center


**Set DNS Nameservers on Ubuntu 18.04**

In order for the Jetson to have access to Internet, the DNS Nameservers must be 
permanently set. This is done with the follwing steps.

* Use this command in the linux terminal to open and edit the :code:`/etc/resolvconf/resolv.conf.d/head`:

    .. code-block:: 

        $ sudo nano /etc/resolvconf/resolv.conf.d/head

* Add the following lines to it:

    .. code-block:: 

        nameserver 8.8.8.8 
        nameserver 8.8.4.4

* Save the file with :code:`CTRL + x` and reboot the system.

* Check the connection with command :code:`ping google.com` in the linux terminal.



Safe shutdown of Jetson Nano
----------------------------

To avoid corrupting files, one should shut down the Jetson Nano through the terminal. If there is power loss while the system is running, 
important system files may get corrupted. It should be noted that that SD card is not encrypted, and it should be possible to extract 
user files even if the Ubuntu OS won’t boot anymore.

The Jetson Nano has to be shut down by entering a terminal command in VSCode.

Enter this command in VSCode SSH session

.. code-block:: 

    $ sudo shutdown now

Enter the password when prompted.



Installing IRImagerDirect SDK
-----------------------------

* Download the correct Linux driver from: `http://evocortex.org/downloads/`_
  The driver must be saved locally in Linux. 

* Do the basic installation as follows:
    :code:`$ sudo dpkg -i libirimager-<VERSION>-<ARCH>.deb`

* Check the installation by running the following command in the Linux terminal: 
    :code:`$ ir_find_serial`

    If :code:`device_not_attached` is written out in the terminal, the software is installed. 

