
.. image:: ./figs/logo-elkem.svg
   :width: 300cm
   :align: left

|
.. image:: ./figs/yi2_logo.svg
   :width: 200cm
   :align: center

|
|
|
Tapping beam detection and measuremnt with IR Camera
====================================================

**A pre-study for UIA M.Sc Mechantronics master thesis**


Author
------
Jørgen Nilsen, `YI2 <https://www.yi2.no/>`_ 


Supervisor
----------
Sondre S. Tørdal, `MotionTech <https://motiontech.no/>`_


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/project_description
   src/prestdy
   src/hardware
   src/main_project
   


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
