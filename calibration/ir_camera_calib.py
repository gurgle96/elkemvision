import numpy as np
import cv2
import glob

img = cv2.imread('C:\\Gitlab\\elkemvision\\calibration\\calibration_images\\frame6.png') # Load image from folder

# Convert to from BGR to grayscale
gray = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

# gray = cv2.equalizeHist(gray)

	 
blur = cv2.GaussianBlur(gray,(3,3),0)
smooth = cv2.addWeighted(blur,0.5,gray,1.5,10)

kernel = np.ones((5,5),np.uint8) # Kernel for the 2D convolution process
closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel) # Removes white specs from objects with black background 
opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel) # Removes black specs from white objects

cv2.imshow('threshold',smooth)
cv2.waitKey(0)

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((6*7,3), np.float32)
objp[:,:2] = np.mgrid[0:7,0:6].T.reshape(-1,2)

# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.

images = glob.glob('*.png')

# for fname in images:

# Find the chess board corners
ret, corners = cv2.findChessboardCorners(img,(7,6)) # Finds corners in the chessboard, performs a adaptive thresholding to create a binary image.

print(ret)
# If found, add object points, image points (after refining them)
if ret == True:
    objpoints.append(objp)

    corners2 = cv2.cornerSubPix(gray,corners,(11,11),(-1,-1),criteria)
    imgpoints.append(corners2)

    # Draw and display the corners
    img = cv2.drawChessboardCorners(gray, (7,6), corners2,ret)
    cv2.imshow('img',img)
    cv2.waitKey(0)

cv2.destroyAllWindows()