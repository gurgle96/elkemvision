from src import IRCamera, BeamDetector, FeatureTracking, Database, RansacFit
from datetime import datetime
from time import sleep, time
import numpy as np
import cv2
import os

path = os.path.dirname(os.path.abspath(__file__))

# Template root folder
template_root = 'template/frame2904.png'

# Initialize variables
thresh_gain = 500  # binary image threshold gain, tune this value between 0 and 255 for better binary representation of tapping beam      
heigth_top = 0 # top heigth of the beam shape edge detection algorithm
heigth_bottom = 0 # bottom heigth of the beam shape edge detection algorithm
thresh_inlier = 3 # inlier distance threshold for the RANSAC algorithm
iterations = 1000 # number of iterations in the RANSAC algorithm
samples = 4 # number of samples per iteration to fit the RANSAC model
inliers = 50 # minimum number of inliers for the RANSAC model


# Create instance of classes
beamDetector = BeamDetector.BeamDetector(thresh_gain, heigth_top, heigth_bottom) 

featureTracking = FeatureTracking.FeatureTracking(template_root)      

ransacFit = RansacFit.RansacFit(thresh_inlier, iterations, samples, inliers)

irCamera = IRCamera.IRCamera(path)

path_db = os.path.join(path,'webapp/database/data.db' )
database = Database.Database(path_db)


# Frame dimensions
framerate = 10
frame_width = 384
frame_heigth = 288

# Create videowriter as a SHM source, two images side-by-side
out = cv2.VideoWriter("appsrc ! video/x-raw, format=BGR ! queue ! videoconvert ! video/x-raw, format=BGRx ! nvvidconv ! omxh264enc ! video/x-h264, stream-format=byte-stream ! h264parse ! rtph264pay pt=96 config-interval=1 ! udpsink host=0.0.0.0 port=5000",  0, framerate, (frame_width*2, frame_heigth))

# Initiate loop parameters
i = 0
k = 0
beam_existence_old = 1
beam_existence_old_old = 1

if __name__ == '__main__':

    # Compile RansacFit.py with JIT
    ransacFit.InitJIT((frame_heigth, frame_width))

    # Check camera status 
    status = irCamera.status()

    while status is True:

        # Add sleep time to the program - used to lower the CPU usage
        sleep(0.2 - time() % 0.2)

        # Get thermal image and array from IR camera
        image_thermal, array_temp = irCamera.getThermal()

        
        # Find the tapping beam region and check for existence of the tappingbeam
        loc, image_tapping, beam_existence = featureTracking.TemplateMatching(image_thermal)
        
        # Calculate the average of the beam existence parameter, used to filter out transient values
        beam_average_existence = np.mean([beam_existence, beam_existence_old, beam_existence_old_old])

        if beam_average_existence == 1:

            # Equalize the histogram of the image 
            image_eq = beamDetector.HistogramEqual(image_tapping)    

            # Create a binary image
            image_binary = beamDetector.BinaryImage(image_eq)

            # Calculate the beam-shape paraemters
            beam_data, beam_status = beamDetector.BeamShape(loc, image_binary)

            
            # Use RansacFit to find the inliers
            inliers,line_xy, image_inliers, theta_line = ransacFit.FindInliers(image_binary)


            # Detect mean, max and min temperature in the tappingbeam
            temp_max, temp_mean, temp_min = beamDetector.TempCalculation(array_temp, image_inliers)

            # Convert to BGR format
            image_inliers = cv2.cvtColor(np.array(image_inliers), cv2.COLOR_GRAY2BGR)

            # Horizontaly stack the images before sending them to the GStreamer pipeline 
            image_GST = np.hstack((image_thermal, image_inliers))
            
        else: 
            
            # Thermal image side-by-side, if the template matching fails
            image_GST = np.hstack((image_thermal,image_thermal))

            # Initialize the remaining values
            temp_max, temp_mean, temp_min = 0,0,0
            inliers, line_xy, theta_line, beam_data, beam_status = 0, 0, 0, 0, 0

        # Write to SHM
        out.write(image_GST)

        # Save data to database, for every 5th iteration
        if i >= 5:
            # Time
            timestamp = datetime.now().replace(microsecond=0)

            # Update temperature data
            database.update_temperature(( temp_max, temp_mean, temp_min, timestamp ))

            # Update image date
            database.update_imagedata(( inliers, line_xy, theta_line, beam_existence, beam_data, beam_status, timestamp ))

            i = 0
        
        i = i + 1

        # Update existence parameter
        beam_existence_old = beam_existence
        beam_existence_old_old = beam_existence_old
        

                   
 