# Tapping beam recognition and measurement with infrared camera

This repository provides the code base for the 2021 Mechatronics master's thesis at the University of Agder. 
The project is done on behalf of Elkem; with a focus on process optmisation through the use of an IR camera.

## Description

This project aims to solve the challenge of using an IR thermal imaging camera for sensing parameters from an industrial process, specifically the tapping beam of molten ferrosilicon from the furnace into the ladle. Existing software for image processing, application development, and data-server communication is implemented as tools to help develop detection and display the parameters of interest. The IR camera is used to detect process abnormalities and log temperature data of the tapping beam.

An application that combines computer vision algorithms with a web-based HMI frontend is developed. 

The application is strucutred into two seperate executable files: 

* `mainRealTime.py` and `mainFlask.py`


## Getting Started

### Dependencies

* Opencv 4.5, or newer.
* Python 3, or newer.
* SQLite3
* Numba
* Flask
* For Linux-based OS, the  [IR Imager SDK from Evocortex](http://documentation.evocortex.com/libirimager2/html/Installation.html), must be installed.    

<!-- ### Installing -->


### Executing program

To run use the application, the above mentioned dependencies must be installed correctly.
Then after cloning/forking the project repository, and a connection is established to the IR camera, the application can be launched.

* Starting with `mainRealTime.py`:
```
python3 mainRealTime.py
```

* If the initiation is successful, this line is printed to the terminal. 
```
===== IR camera ready ======
``` 

* Then, the HMI can be started with:

```
python3 mainFlask.py
```

## Authors

Jørgen Nilsen(https://joergennilsen.no)


## Acknowledgments

Inspiration, code snippets, etc.
* [RANSAC](https://medium.com/@iamhatesz/random-sample-consensus-bd2bb7b1be75)
* [OpenCV-Python Tutorials](https://docs.opencv.org/master/d6/d00/tutorial_py_root.html)

Thank you to the supervisor on this master's project, [Sondre Sanden Tørdal](https://sondrest.gitlab.io/portfolio/). 
