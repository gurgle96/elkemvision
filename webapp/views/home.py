#!/usr/bin/env python

from webapp import app, path
from src import Database
from flask import render_template, url_for, Response
import numpy as np
import cv2
import io
import random
import json
import time
from datetime import datetime
import os




# Connect to database
path_db = os.path.join(path,'database','data.db' )
database = Database.Database(path_db)

# Capture UDP stream
cap = cv2.VideoCapture("udpsrc port=5000 ! application/x-rtp ! queue ! rtph264depay ! h264parse ! nvv4l2decoder ! nvvidconv ! videoconvert ! appsink")

# Defines the 
beam_angle_good = np.array([0,72])
beam_angle_bad = np.array([72,120])

# Yield images
def frame_thermal():
    while True:
        ret, frame = cap.read()
        frame_split = frame[:,0:383] 
        if frame_split is not None:
            ret, frame_split = cv2.imencode('.jpg', frame_split)
            frame_split = frame_split.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame_split + b'\r\n')

def frame_binary():
    while True:
        ret, frame = cap.read()
        frame_split = frame[:,383:767]
        if frame_split is not None:
            ret, frame_split = cv2.imencode('.jpg', frame_split)
            frame_split = frame_split.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame_split + b'\r\n')

def frame_shape():
    while True:
        _, beam_data = database.select_data()
        beam_existence = np.mean(np.array([beam_data[0][4], beam_data[1][4], beam_data[2][4], beam_data[3][4], beam_data[4][4]]))
        beam_status = np.mean(np.array([beam_data[0][6], beam_data[1][6], beam_data[2][6], beam_data[3][6], beam_data[4][6]]))


        if beam_existence == 1 and beam_status == 1:
            frame = cv2.imread('webapp/static/HMI_beam_shape_optimal.png',1)

        elif beam_existence == 1 and beam_status == 0:
            frame = cv2.imread('webapp/static/HMI_beam_shape_split.png',1)

        elif beam_existence == 0:
            frame = cv2.imread('webapp/static/HMI_beam_shape_stopped.png',1)

        else:
            frame = None

        if frame is not None:
            ret, frame = cv2.imencode('.png', frame)
            frame = frame.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')
        

def frame_angle():

    while True:
        _, beam_data = database.select_data()
        beam_existence = np.mean(np.array([beam_data[0][4], beam_data[1][4], beam_data[2][4], beam_data[3][4], beam_data[4][4]]))
        beam_angle = np.mean(np.array([beam_data[0][3], beam_data[1][3], beam_data[2][3], beam_data[3][3], beam_data[4][3]]))
        
        if beam_existence == 1 and beam_angle_good[0] < beam_angle <= beam_angle_good[1]:
            frame = cv2.imread('webapp/static/HMI_beam_angle_free_pouring.png',1)

        elif beam_existence == 1 and beam_angle_bad[0] < beam_angle <= beam_angle_bad[1]:
            frame = cv2.imread('webapp/static/HMI_beam_angle_sticking.png',1)

        elif beam_existence == 0:
            frame = cv2.imread('webapp/static/HMI_beam_shape_stopped.png',1)

        else:
            frame = None


        if frame is not None:
            ret, frame = cv2.imencode('.png', frame)
            frame = frame.tobytes()
            yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/')
def index():
    """ Home template """
    return render_template('home.html')


@app.route('/camera_feed_thermal')
def camera_feed_thermal():
    """Video streaming route."""
    return Response(frame_thermal(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/camera_feed_binary')
def camera_feed_binary():
    """Video streaming route."""
    return Response(frame_binary(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/beam_status_image')
def beam_status_image():
    """ Beam status image """
    return Response(frame_shape(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/beam_angle_image')
def beam_angle_image():
    """ Beam angle image """
    return Response(frame_angle(), mimetype='multipart/x-mixed-replace; boundary=frame')


@app.route('/beam-temp')
def beam_temp():
    """ Temperature-data route """
    def generate_temperature_data():
        while True:
            temp_data, _ = database.select_data()
            json_data = json.dumps(
                {'time': temp_data[0][4], 'value': temp_data[0][1:4]})
            yield f"data:{json_data}\n\n"
            time.sleep(1)

    return Response(generate_temperature_data(), mimetype='text/event-stream')







