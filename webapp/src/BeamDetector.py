import numpy as np
import cv2


class BeamDetector(object)

    def __init__(self):


    # Inspired from: https://github.com/andridns/cv-strawberry
    # Colourspaces and object tracking: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_colorspaces/py_colorspaces.html#converting-colorspaces
    def Threshold(self,image):
        # Convert from BGR to RGB
        image_RGB = cv2.cvtColor(image_original, cv2.COLOR_BGR2RGB)

        # Blur image slightly
        image_RGB = cv2.GaussianBlur(image_RGB, (5, 5), 0)

        # Convert to HSV
        image_HSV = cv2.cvtColor(image_RGB, cv2.COLOR_RGB2HSV)

        # Lower range for white
        white_lower = np.array([0,0,245])

        # Upper range for white
        white_upper = np.array([0,0,255])

        # Threshold HSV image to get only white colours
        mask = cv2.inRange(image_HSV, white_lower, white_upper)

        # Fill small gaps and remove specks
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (5, 5))
        image_closed = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        image_closed_then_opened = cv2.morphologyEx(image_closed, cv2.MORPH_OPEN, kernel)

        # Finds the coordinates where the pixel value is 255
        indices = np.where(image_closed_then_opened == 255)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        # Create new image from the original image
        image_black = image_original

        # Convert to black image
        image_black = np.ones(image_black.shape)

        # Isolate the tapping beam ROI from the original image
        tapping_beam = image_original[coordinates_x,coordinates_y]

        # Add the isolated ROI to the black image
        image_black[coordinates_x,coordinates_y] = tapping_beam

        return(image_black,image_original)


    def HistogramEqual(self,image):

        # Read image, create grayscale
        image_original = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Find where the grayscale is bigger than or equal to 200 
        indices = np.where(image_original >= 200)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        # Create new image from the original image
        image_white = image_original

        # Convert to white image
        image_white = np.zeros(image_white.shape,np.uint8)

        # Isolate the tapping beam region from the original image
        tapping_beam = image_original[coordinates_x,coordinates_y]

        # Add the isolated ROI to the black image
        image_white[coordinates_x,coordinates_y] = tapping_beam

        # Equalize the histogram
        image_white = cv2.equalizeHist(image_white)

        # Blur image slightly
        #image_white = cv2.GaussianBlur(image_white, (3, 3), 0)

        # Erosion
        kernel = np.ones((1,1),np.uint8)
        erosion = cv2.erode(image_white,kernel,iterations = 1)

        # Fill small gaps and remove specks
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        image_closed = cv2.morphologyEx(erosion, cv2.MORPH_CLOSE, kernel)
        image_closed_then_opened = cv2.morphologyEx(image_closed, cv2.MORPH_OPEN, kernel)

        # Dilation
        kernel = np.ones((4,4),np.uint8)
        dilation = cv2.dilate(image_closed_then_opened,kernel, iterations = 1)

        # Threshold the image
        _, image_threshold = cv2.threshold(dilation,240,255,cv2.CHAIN_APPROX_NONE)

        return(image_threshold)
