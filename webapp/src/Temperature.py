import numpy as np
import cv2


class Temperature(object):
    """
    Temperature object takes in the temperature matrix and image of detected tappingbeam,
    calculates the average, max and min temperature. 
    """

    __init__(self):


    