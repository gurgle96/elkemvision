#!/usr/bin/env python

from flask import Flask
import os

# Flask app instance
app = Flask(__name__)

path = os.path.dirname(os.path.abspath(__file__))
print(path)
import webapp.views

