from src import BeamDetector, FeatureTracking, RansacFit
import cv2
import os


# Root Folders - add data here to test the system offline
image_root = '/home/elkem/GitLab/elkemvision/Test_tapping' # Tapping test - Lenovo 
# image_root = 'C:\\Users\\bruker\\Desktop\\Dataset1\\original_image' # Water test - Lenovo
# image_root = 'C:\\Users\\joerg\\OneDrive\\Skrivebord\\Test_tapping' # Tapping test - Dell
template_root = '/home/elkem/GitLab/elkemvision/template/template.png'


# Initialize variables
thresh_gain = 500  # binary image threshold gain, tune this value between 0 and 255 for better binary representation of tapping beam      
heigth_top = 0 # top heigth of the beam shape edge detection algorithm
heigth_bottom = 0 # bottom heigth of the beam shape edge detection algorithm
thresh_inlier = 3 # inlier distance threshold for the RANSAC algorithm
iterations = 1000 # number of iterations in the RANSAC algorithm
samples = 4 # number of samples per iteration to fit the RANSAC model
inliers = 50 # minimum number of inliers for the RANSAC model


# Create instance of classes
beamDetector = BeamDetector.BeamDetector(thresh_gain, heigth_top, heigth_bottom) 

featureTracking = FeatureTracking.FeatureTracking(template_root)      

ransacFit = RansacFit.RansacFit(thresh_inlier, iterations, samples, inliers)


i = 3    

if __name__ == '__main__': 
    while i<4:

        # Read image - soon to be replaced by IRCamera.py
        image = cv2.imread(os.path.join(image_root, 'test_frame{}.tiff'.format(i))) 

        # image_binary = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 

        # Find the tapping beam region
        loc, image_tapping, beam_existence = featureTracking.TemplateMatching(image)

        # Equalize the histogram of the  image 
        image_eq = beamDetector.HistogramEqual(image_tapping)

        # Create a binary image and use the location of the template matching to only select the ROI of the tappingbeam
        image_binary = beamDetector.BinaryImage(image_eq)

        # Use RansacFit to find the inliers
        inliers,line_xy,image_inliers, theta_line = ransacFit.FindInliers(image_binary)
      

        # Calculate the beam-shape paraemters
        beam_data, beam_status = beamDetector.BeamShape(loc, image_binary)
        

        # Draw a boundingbox around the tappingbeam
        image_contour =  featureTracking.BoundingBox(   image,  # image to place boundingbox around
                                                        loc,    # location data of the tappingbeam
                                                        theta_line)    # parallelogram offset angle

        

        # Create BGR image for displaying side-by-side with the original image   
        # image_binary = cv2.cvtColor(image_binary,cv2.COLOR_GRAY2BGR)

        # image = np.hstack((image_contour,image_binary))

        print('Theta line:',theta_line)
        print('Beam_data:',beam_data)

        # cv2.imshow('test',image) 
        
        # cv2.imshow('RANSAC',image_inliers.astype(np.uint8))

        # cv2.waitKey(0)


        i = i + 1    

