import numpy as np
import cv2


class Temperature(object):

    def TempCalculation(self, array_thermal, image_binary):
        """
        Calculates the mean, max and min temperature of the tapping beam
        """
        
        # Find the pixel coordinates for the tapping beam
        indices = np.where(image_binary == 255)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        # Empty array
        temp_region = np.zeros(array_thermal.shape, np.uint8)      

        # Isolate the tapping beam temperature
        temp_region = array_thermal[coordinates_x, coordinates_y]

        # Calcualte mean, max and min temperature
        temp_mean = np.mean(temp_region)
        temp_max = np.max(temp_region)
        temp_min = np.min(temp_region)

        return temp_mean, temp_max, temp_min


