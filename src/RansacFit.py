import numpy as np
import numba as nb
import cv2
from numba import jit
from numba.experimental import jitclass


spec = [('thresh',nb.float32),
        ('iterations',nb.uint32),
        ('samples',nb.uint32),
        ('inliers',nb.uint32)]
@jitclass(spec)
class RansacFit(object):

    def __init__(self, thresh = 3, iterations = 1000, samples = 4, inliers = 50):

        """
        Initialize the variables for RansacFit(thresh),
        where thresh is the inlier threshold for the RANSAC algorithm.
        """
        self.thresh = np.float32(thresh)
        self.iterations = np.uint32(iterations)
        self.samples = np.uint32(samples)
        self.inliers = np.uint32(inliers)

    def InitJIT(self, data_size):

        """
        Initializes the JIT - compiler.
        Run this function directly after the instansiation of the RansacFit class. 
        Note that the data input to this function should be of the same size and type 
        as the data that is to be used in the FindInliers function
        """
        # Create sample data
        data = np.ones(data_size, dtype = np.uint32)
        
        data[30:150,150:170] = np.uint32(255)
        data[100:150,100:150] = np.uint32(255)

        # Rund the Ransac function with sample data
        initInliers, _, _, _ = self.FindInliers(data)

        # If sucsessfull, print to terminal
        if initInliers is not None:
            print('===== JIT compiled RansacFit.py =====')


    def FindInliers(self, data):

        """
        Finds the inliers in a dataset based upon a y=ax+b line model. 
        Takes in a binary image of the segmented tappingbeam, returns the 
        position for.
        """

        # Find the matrix positions where data == 255
        indices = np.where(data == 255)

        # Converts the coordinates
        data_x, data_y = np.transpose(indices[1]), np.transpose(indices[0])

        # Create data matrix - flipped axis for using image coordinate system
        data_xy = np.column_stack((data_x,data_y))

        # Robustly find inlier data with self written RANSAC algorithm
        bestModel, bestInliers= self.fit_ransac(data_xy, max_iters = self.iterations, samples_to_fit = self.samples, inlier_threshold = self.thresh, min_inliers = self.inliers)

        # Generate coordinates of estimated line model
        line_x_min = np.uint32(np.min(bestInliers[:,0])) # lowest index value
        line_x_max = np.uint32(np.max(bestInliers[:,0])) # highest index value

        line_x = np.arange(start = line_x_min, stop = line_x_max, dtype = np.float64) # arange integers from lowest to highest
        length_x = len(line_x) # length of line
        line_y = np.zeros(length_x, dtype = np.float64) # empty zeros vector for y-value
        

        bestModel_a = bestModel[0,0]
        bestModel_b = bestModel[1,0]*np.ones(length_x, dtype = np.float64)
        line_y = bestModel_a*line_x + bestModel_b

        # Calculate the absolute angle for the line model
            # startPoint is supposed to be the point on the RANSAC'ed line 
            # that is closest to the tappinspout (top right corner).  
            # endPoint is supposed to be the point on the RANSAC'ed line 
            # at the bottom of the image.

        if line_y[0] == np.max(line_y):
            startPoint = (line_x[-1], line_y[-1])
            endPoint = (line_x[0], line_y[0])

        elif line_y[0] == np.min(line_y):
            startPoint = (line_x[0], line_y[0])
            endPoint = (line_x[-1], line_y[-1])            

        # Find the delta-distance between startPoint and endPoint in x- and y-direction
        deltaX = (endPoint[0] - startPoint[0])
        deltaY = (endPoint[1] - startPoint[1])
    
        theta_deg = np.arctan2(deltaY,deltaX)*180/np.pi

    
        # Base of the binary image
        image_binary = np.zeros(data.shape, dtype = np.uint8) 
        image_white = np.uint8(255)
        
        # Create binary image of the inliers
        index_length = len(bestInliers[:,0])
        index_x = np.zeros((index_length,1), dtype = np.uint32)
        index_y = np.zeros((index_length,1), dtype = np.uint32)
        index_x[:,0] = bestInliers[:,0]
        index_y[:,0] = bestInliers[:,1]

        # Iterate through the image matrix
        for ii in range(index_length):
            image_binary[index_y[ii,0], index_x[ii,0]] = image_white

        image_binary = np.copy(image_binary)
        
        return bestInliers, bestModel, image_binary, theta_deg



    def fit_lsq(self, X, y):

        """
        Source: https://medium.com/@iamhatesz/random-sample-consensus-bd2bb7b1be75
        Fits model for a given data using least squares.
        X should be an mxn matrix, where m is number of samples, and n is number of independent variables.
        y should be an mx1 vector of dependent variables.
        """
        
        b = np.ones((X.shape[0], 1), np.uint32)
        A = np.hstack((X, b)).astype(np.float64)
 
        theta = np.linalg.lstsq(A, y.astype(np.float64), rcond=-1)[0] 

        return theta


 
    def evaluate_model(self, X, y, theta, inlier_threshold):
        """
        Source: https://medium.com/@iamhatesz/random-sample-consensus-bd2bb7b1be75

        Evaluates model and returns total number of inliers.
        X should be an mxn matrix, where m is number of samples, and n is number of independent variables.
        y should be an mx1 vector of dependent variables.
        theta should be an (n+1)x1 vector of model parameters.
        inlier_threshold should be a scalar.
        """

    
        X = X.copy() 
        b = np.ones((X.shape[0], 1), np.uint32)
        y = y.copy() 
        A = np.hstack((y, X, b))
        theta = np.append(-1., theta) 
        
        distances = np.abs(np.sum(A*theta, axis=1)) / np.sqrt(np.sum(np.power(theta[:-1], 2)))
        inliers = (distances <= inlier_threshold).astype(np.bool_)
        num_inliers = np.count_nonzero(inliers)

        return num_inliers, inliers

            

    def fit_ransac(self, data, max_iters, samples_to_fit, inlier_threshold, min_inliers):
        """
        Source: https://medium.com/@iamhatesz/random-sample-consensus-bd2bb7b1be75
        """
        # Create X and y vector from the dataset
        data_len = len(data[:,0])
        X = np.zeros((data_len,1), dtype = np.uint32)
        y = np.zeros((data_len,1), dtype = np.uint32)
        X[:,0] = data[:,0]
        y[:,0] = data[:,1]

        # Declare variables for RANSAC algorithm
        best_model = np.zeros((2,1),dtype = np.float64)
        best_inliers = np.zeros((X.shape[0], 1), dtype = np.bool_)
        best_model_performance = np.uint32(0)
        num_samples = X.shape[0]

       
    
        # Iterate through the data set with random points
        for i in range(max_iters):
            sample = np.random.choice(num_samples, size=samples_to_fit, replace=False)
            model_params = self.fit_lsq(X[sample], y[sample])
            model_performance, model_inliers = self.evaluate_model(X, y, model_params, inlier_threshold) 
            
            if model_performance < min_inliers:
                continue
            
            if model_performance > best_model_performance:
                best_model = model_params
                best_model_performance = model_performance
                best_inliers[:,0] = model_inliers
                

        # Find the index for each inlier, achieve proper array dimensions
        best_inliers_index = np.zeros((best_model_performance, 1), dtype = np.uint32) # create zeros vector
        best_inliers_index[:,0] = np.where(best_inliers)[0].astype(np.uint32) # transfer the inlier indexes into the new vector

        inliers_x = np.zeros((best_model_performance,1), dtype = np.uint32) # create zeros vector
        inliers_y = np.zeros((best_model_performance,1), dtype = np.uint32) # create zeros vector

        inliers_x[:] = X[best_inliers_index.reshape(best_model_performance)] # transer image position of the inliers in x-direction
        inliers_y[:] = y[best_inliers_index.reshape(best_model_performance)] # transer image position of the inliers in y-direction    

        inliers_xy = np.hstack((inliers_x, inliers_y)) # combine the inliers into a nx2 array 


        return best_model, inliers_xy

