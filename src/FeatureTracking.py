
import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
import time


class FeatureTracking(object):
    """
    Class for tracking the tapping beam, uses template matching finding the tapping beam region.
    """

    def __init__(self,templatePath):
        """
        Initialize variables
        """
        self.templatePath = templatePath


    def TemplateMatching(self,image):
        """
        Template matching for finding the specific image region where the tapping beam is located.
        Requires a template of the tapping beam, use Template.py to create the desired template image.
        """
        # Read template
        template_image = cv2.imread(self.templatePath)

        # Convert template image to grayscale
        template_image = cv2.cvtColor(template_image,cv2.COLOR_BGR2GRAY)

        # Convert CV image to grayscale     
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 

        # Get template height and width
        template_w, template_h = template_image.shape[::-1]

        # Get image height and width
        image_w, image_h = image_gray.shape[::-1]

        # Convert CV image to grayscale     
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 

        # Perform match operations 
        res = cv2.matchTemplate(image_gray,template_image,cv2.TM_CCOEFF_NORMED) 
        # Specify a threshold for the matching 
        threshold = 0.8
        
        # Store the coordinates of matched area in a numpy array 
        loc = np.where( res >= threshold)[::-1]

        # Base of the segmented image
        image_segmented = np.zeros(image_gray.shape,np.uint8)

        if bool(loc[0].any()) is False:
            loc = (0, 0, image_w, image_h)
            beam_existence = 0
            
            # Isolate the tapping ROI
            image_segmented = image_segmented


        else:
            loc = (int(round(np.mean(loc[0]),0)),int(round(np.mean(loc[1]),0)),template_w, template_h) 
            beam_existence = 1
            # Isolate the tapping ROI
            image_segmented[loc[1]:(loc[1] + loc[3]), loc[0]:(loc[0] + loc[2])] = image_gray[loc[1]:(loc[1] + loc[3]), loc[0]:(loc[0] + loc[2])]
 

        return loc, image_segmented, beam_existence


    def BoundingBox(self,image,loc,theta):
        """
        Draw a bounding box over the tappingbeam grayscale image.
        The coordinates for the bounding box is numbered ccw, 
        starting with the top left corner.
        """
        
        # Define top left corner, width and heigth
        pt0, width, heigth = (loc[0],loc[1]), loc[2], loc[3]

        # Calculate the absolute value of the tapping beam angle
        absDelta = np.ceil(np.abs(heigth/np.tan(theta[0])))

        # Correct the offset 
        if theta[1] < 90: 
            delta = absDelta
        else: 
            delta = - absDelta

        # Define corners
        pt1 = [pt0[0], pt0[1]]
        pt2 = [pt0[0] + delta, pt0[1] + heigth]
        pt3 = [pt0[0] + width + delta, pt0[1] + heigth]
        pt4 = [pt0[0] + width, pt0[1]]

        # Create list of corners
        corners = [np.array([ pt1, pt2, pt3, pt4 ], dtype = int)] 

        # Draw the contour
        image_contour = cv2.drawContours(np.array(image),corners,-1,(255,255,255),2)

        return image_contour

    
    


    