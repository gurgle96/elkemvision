import numpy as np



class BeamDetector(object):

    def __init__(self,thresh_gain = 100,heigth_top = 0, heigth_bottom = 0):
        """
        Initialize the variables.
        """
        # BinaryImage
        self.thresh_gain = thresh_gain

        # BeamShape
        self.heigth_top = heigth_top # starting position for the edge detection
        self.heigth_bottom = heigth_bottom # djust stop position for the edge detetion

    def BinaryImage(self,image):
        """
        Binary image thresholding based upon temperature/image brightness. 
        This algorithm is based upon the idea that the tapping beam always 
        exists in the brightest image pixels, within some margin.
        """
        # Base of the binary image
        image_binary = np.zeros(image.shape,np.uint8)
        image_white = np.uint8(255)

        # Find the pixels with maximum brightness
        i_mean = np.mean(image)
        i_max = np.amax(image)
        i_thresh = np.ceil(i_max - i_mean/i_max*self.thresh_gain)
        indices = np.where(image >= i_thresh) 

        # print('Threshold:',i_thresh,'Max:',i_max,'Mean:',i_mean)

        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        image_binary[coordinates_x,coordinates_y] = image_white

        return image_binary



    def HistogramEqual(self,image):
        """
        Function for normalizing the histogram
        https://www.imageeprocessing.com/2011/04/matlab-code-histogram-equalization.html
        """

        # Create histogram vector
        histogram = np.zeros(256)
        buffer, _ = np.histogram(image,
                                bins=np.arange(256),
                                range=(0, 1))

        histogram[0:buffer.size]=buffer

        # Cumulative distrobution of the histogram
        cdf = histogram.cumsum() 

        # Create masked array, excluding indices where the value is zero
        cdf_m = np.ma.masked_equal(cdf,0) 

        # Scale the histogram to values between 0 and 255
        cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min()) 

        # Fill inn the blank indexes with the value = 0
        cdf = np.ma.filled(cdf_m,0).astype('uint8')

        # Create the new equalised image
        image_eq = cdf[image]

        return image_eq

    def BeamShape(self, loc, image):
        """
        Slices the tapping beam into horizontal vectors with even spacing, 
        detects the edges by convolving a filter with the image vector. 
        """
        # Define top left corner, width and heigth
        pt0, width, heigth = (loc[0],loc[1]), loc[2], loc[3]

       
        # Isolate the pixels of the tappingbeam, found with template. 
        image_isolated = np.zeros((heigth,width))
        image_isolated = image[pt0[1]:(pt0[1] + heigth), pt0[0]:(pt0[0] + width)]

        # For local testing
        # image_isolated[:,10:20] = 0 
        # image_isolated[:,30:40] = 0

        # Increment vector for iterating through the image in the y-direction
        y_increment = np.arange(self.heigth_top, (heigth + self.heigth_bottom), 10)
        
        # Empty array for storing the y-position and the detected number of edges
        beam_data = np.zeros((len(y_increment),2))

        # Initialize counter variable
        k = 0

        # Iterating through the image in the y-direction, row-by-row.
        for i in y_increment:
            
            # Convolve the extracted image data with a Gaussian derivative filter
            row = image_isolated[i,:]
            row_convolve = np.convolve([1,0,-1], row)

            # Find peaks in dataset, inspired by: https://tcoil.info/find-peaks-and-valleys-in-dataset-with-python/
            row_convolve_peak = (np.diff(np.sign(np.diff(np.abs(row_convolve)))) < 0).nonzero()[0] + 1 
            
            # Count number of edges in row
            edges = len(row_convolve_peak)

            # Save data
            beam_data[k,:] = np.array([i,edges])

            # Update counter
            k += 1

        # Remove noise, only accept data between 0 and 8
        indices = np.where( beam_data[:,1]>8 )
        coordinates_x = np.transpose(indices[0])

        # Set the invalid values to zero
        beam_data[coordinates_x, 1] = 0

        # Calculate the mean of the tappingbeam shape-values
        beam_value_mean = np.mean(beam_data[:,1])

        if 3 >= beam_value_mean > 1:

            beam_status = 1 # Optimal shape

        elif beam_value_mean > 3:

            beam_status = 0 # Sparse shape

        else:
            
            beam_status =  2 # Fault, check beam_data matrix or adjust the y-increment vector in BeamDetector.BeamShape()

        return beam_data, beam_status
    


    def TempCalculation(self, array_thermal, image_binary):
        """
        Calculates the mean, max and min temperature of the tapping beam.
        """
        
        # Find the pixel coordinates for the tapping beam
        indices = np.where(image_binary == 255)
        
        # Converts the coordinates
        coordinates_x = np.transpose(indices[0])
        coordinates_y = np.transpose(indices[1])

        coordinates_y = np.delete(coordinates_y, np.where(coordinates_y >= 382))

        # Empty array
        temp_region = np.zeros(image_binary.shape, np.uint8)
        
        # Isolate the tapping beam temperature
        temp_region = array_thermal[coordinates_x, coordinates_y]

        # Calcualte mean, max and min temperature
        temp_mean = np.mean(temp_region)
        temp_max = np.max(temp_region)
        temp_min = np.min(temp_region)

        return temp_max, temp_mean, temp_min

