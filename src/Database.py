from sqlite3 import Error
from datetime import datetime
import numpy as np
import sqlite3
import io
import os

class Database(object):


    def __init__(self,dbPath):
        """
        Initalize variables
        """
        self.dbPath = dbPath

        # Register a new datatype
        def adapt_array(arr):
            """
            http://stackoverflow.com/a/31312102/190597 (SoulNibbler)
            """
            out = io.BytesIO()
            np.save(out, arr)
            out.seek(0)
            return sqlite3.Binary(out.read())

        def convert_array(text):
            out = io.BytesIO(text)
            out.seek(0)
            return np.load(out)

        # Converts np.array to TEXT when inserting
        sqlite3.register_adapter(np.ndarray, adapt_array)

        # Converts TEXT to np.array when selecting
        sqlite3.register_converter("array", convert_array)

        # Temperature data table
        sql_create_temperature_table = """ CREATE TABLE IF NOT EXISTS temperature ( id              integer PRIMARY KEY,
                                                                                    tempMax         float                   NOT NULL,
                                                                                    tempMean        float                   NOT NULL,
                                                                                    tempMin         float                   NOT NULL,
                                                                                    timestamp       text                    NOT NULL
                                                                                ); """

        # Image data table
        sql_create_imagedata_table = """ CREATE TABLE IF NOT EXISTS imagedata   (   id              integer PRIMARY KEY,
                                                                                    inliers         arr                         NOT NULL,
                                                                                    lineXY          arr                         NOT NULL,
                                                                                    lineAngle       float                       NOT NULL,
                                                                                    beamExcistence  float                       NOT NULL,
                                                                                    beamShape       arr                         NOT NULL,
                                                                                    beamStatus      float                       NOT NULL,
                                                                                    timeStamp       text                        NOT NULL             
                                                                                ); """

        
        
        # create a database connection
        self.conn = self.create_connection()

        # create tables
        if self.conn is not None:
            # create temperature table
            self.create_table(sql_create_temperature_table)

            # create image data table
            self.create_table(sql_create_imagedata_table)

            print("===== Created connection to database =====")

        else:
            print("Error! cannot create the database connection.")


    def create_connection(self):
        """ create a database connection to a SQLite database """
        conn = None
        try:
            conn = sqlite3.connect(self.dbPath, check_same_thread = False, detect_types=sqlite3.PARSE_DECLTYPES)
            return conn

        except Error as e:
            print(e)

        return conn

    def create_table(self, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = self.conn.cursor()

            c.execute(create_table_sql)

        except Error as e:
            print(e)

    def update_temperature(self, data):

        sql = ''' INSERT INTO temperature(tempMax, tempMean, tempMin, timestamp)
                VALUES(?,?,?,?) '''

        cur = self.conn.cursor()
        cur.execute(sql, data)
        self.conn.commit()
        return cur.lastrowid

    def update_imagedata(self,data):

        sql = ''' INSERT INTO imagedata(inliers, lineXY, lineAngle, beamExcistence, beamShape, beamStatus, timestamp)
                VALUES(?,?,?,?,?,?,?) '''

        cur = self.conn.cursor()

        cur.execute(sql, data)
        
        self.conn.commit()
        return cur.lastrowid

    def select_data(self):

        cur = self.conn.cursor()
        cur.execute("SELECT * FROM temperature ORDER BY id DESC LIMIT 1")

        rows_temperature = cur.fetchall()

        cur.execute("SELECT * FROM imagedata ORDER BY id DESC LIMIT 5")

        rows_imagedata = cur.fetchall()
        
        return rows_temperature, rows_imagedata
  
if __name__ == '__main__':


    database = Database(r'/home/elkem/GitLab/elkemvision/webapp/database/data.db')

    _, beam_data = database.select_data()
    beam_existence = beam_data[0][4]
    beam_angle = beam_data[0][3]
    print(beam_angle)
