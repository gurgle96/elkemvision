#! /usr/bin/env python3
from ctypes.util import find_library
import numpy as np
import ctypes as ct
import cv2
import os
import sys
import time

class IRCamera(object):
    """
    IRCamera object initializes the thermal camera and 
    returns the thermal image and the temperature array
    """
    def __init__(self,source):
        """
        Initialize camera variables
        """       
        # create log dir if non existent
        if not os.path.exists('./src/log'):
            os.mkdir('./src/log')

        if not os.path.exists('./src/images'):
            os.mkdir('./src/images')


        # load library
        if os.name == 'nt':
            #windows:
            path_dll = os.path.join(source, "bin\\x64\\libirimager.dll" )
            self.libir = ct.CDLL(path_dll) 

            #init vars for Windows
            self.pathXml = ct.c_char_p(b'./config/20022030.xml')
            self.pathFormat = ct.c_char_p(b'./config/')
            self.pathLog = ct.c_char_p(b'./log/logfilename')
            print('===== Windows OS =====')

        else:
            print('DLL path:',source)
            #linux:
            self.libir = ct.cdll.LoadLibrary(ct.util.find_library("irdirectsdk"))

            #init vars for Linux
            path_xml = os.path.join(source, "config/20022030.xml" )
            self.pathXml = ct.c_char_p(path_xml.encode()) 
            self.pathFormat = ct.c_char_p()
            path_log = os.path.join(source, "log/logfilename" )
            self.pathLog = ct.c_char_p(path_log.encode())
            print('===== Linux OS =====')


        # init vars for thermal camera
        self.palette_width = ct.c_int()
        self.palette_height = ct.c_int()
        self.thermal_width = ct.c_int()
        self.thermal_height = ct.c_int()
        self.serial = ct.c_ulong()

        # instance of EvoIRFrameMetadata class
        self.metadata = self.EvoIRFrameMetadata()

        # init lib
        self.retLib = self.libir.evo_irimager_usb_init(self.pathXml, self.pathFormat, self.pathLog)
        print('===== Init IR imager library =====')
        if self.retLib != 0:
            print("error at init")
            exit(self.retLib)

        # get the serial number
        ret = self.libir.evo_irimager_get_serial(ct.byref(self.serial))
        print('serial: ' + str(self.serial.value))

        # get thermal image size
        self.libir.evo_irimager_get_thermal_image_size(ct.byref(self.thermal_width), ct.byref(self.thermal_height))
        print('thermal width: ' + str(self.thermal_width.value))
        print('thermal height: ' + str(self.thermal_height.value))

        # init thermal data container
        self.np_thermal = np.zeros([self.thermal_width.value * self.thermal_height.value], dtype=np.uint16)
        self.npThermalPointer = self.np_thermal.ctypes.data_as(ct.POINTER(ct.c_ushort))

        # get palette image size, width is different to thermal image width duo to stride alignment!!!
        self.libir.evo_irimager_get_palette_image_size(ct.byref(self.palette_width), ct.byref(self.palette_height))
        print('palette width: ' + str(self.palette_width.value))
        print('palette height: ' + str(self.palette_height.value))

        # init image container
        self.np_img = np.zeros([self.palette_width.value * self.palette_height.value * 3], dtype=np.uint8)
        self.npImagePointer = self.np_img.ctypes.data_as(ct.POINTER(ct.c_ubyte))


        """
        Initialize calibration variables
        """
        # Initilize camera parameters
        self.f = 0.013  # focal length of the 029 lens on the Optris PI400i
        self.Sx = 384   # camera resolution in the x-axis (horisontal)
        self.Sy = 288   # camera resolution in the y-axis (vertical)

        # Camera intrinsic calibration matrix
        self.mtx =  np.array([[  -self.f/self.Sx,   0,                  self.Sx/2],
                            [   0,                  -self.f/self.Sy,    self.Sy/2],
                            [   0,                  0,                  1]])
        # Camera distortion calibration matrix
        self.dist = 0

        # Splits up calibration into making a map, and doing the remapping in calibrate
        # member function
        # This is faster than doing cv2.undistort()
        self.map1, self.map2 = cv2.initUndistortRectifyMap(
            self.mtx,
            self.dist,
            np.eye(3),
            self.mtx,
            (self.Sx, self.Sy),
            cv2.CV_32FC1
        )

    def status(self):
        """
        Returns a boolean value for the initialization status of the camera
        """

        if self.retLib == 0:
            status = True
            print('===== IR camera ready ======')

        else: 
            status = False
            print('Error at init. Camera status:',status)

        return status
            


    
    def getThermal(self):
        """
        Function for returning the thermal image and temperature array
        """

        ret = self.libir.evo_irimager_get_thermal_palette_image_metadata(self.thermal_width, self.thermal_height, self.npThermalPointer, self.palette_width, self.palette_height, self.npImagePointer, ct.byref(self.metadata))

        if ret != 0:
            print('error on evo_irimager_get_thermal_palette_image ' + str(ret))
            

        # get the palette image
        thermal_image = self.np_img.reshape(self.palette_height.value, self.palette_width.value, 3)[:,:,::-1]

        # get the thermal array
        thermal_vector = self.np_thermal/10. - 100.

        thermal_array = thermal_vector.reshape(self.thermal_height.value, self.thermal_width.value)


        return thermal_image, thermal_array

    def calibrate(self,image):
        """
        Perform calibration of input OpenCV image
        """
        # Make copy of input image

        # # Perform color calibration
        # image_float = np.array(image, float) * self.gain_matrix

        # # Clip to keep values between 0 and 255 
        # # Using [:] sets the value of object "image" without making a new object
        # # i.e. just replaces in-object which does not need a return statement        
        # calibrated[:] = np.clip(image_float, 0, 255)

        # Apply camera distortion calibration
        calibrated = image.copy()
        calibrated[:] = cv2.remap(calibrated, self.map1, self.map2, cv2.INTER_LINEAR)
        return calibrated


    class EvoIRFrameMetadata(ct.Structure):
        """
        Structure that contains additional frame information from the camera
        """
        
        # Define EvoIRFrameMetadata structure for additional frame info        
        _fields_ = [
            ("counter", ct.c_uint),
            ("counterHW", ct.c_uint),
            ("timestamp", ct.c_longlong),
            ("timestampMedia", ct.c_longlong),
            ("flagState", ct.c_int),
            ("tempChip", ct.c_float),
            ("tempFlag", ct.c_float),
            ("tempBox", ct.c_float),
        ]




if __name__ == "__main__":

    path = os.path.dirname(os.path.abspath(__file__))

    # Crop area, tapping test
    class Template():
        x = 145
        y = 40
        w = 80
        h = 230

    xyTemplate = Template()

    # instance of IRCamera class
    irCamera = IRCamera(path)

    # capture and display image till q is pressed
    t, i = 0, 0
    while chr(cv2.waitKey(1) & 255) != 'q':


        # get thermal image and temperature array    
        image_thermal, array_thermal = irCamera.getThermal()

        # Index thermal image array to create template image
        image_template = cv2.cvtColor(image_thermal, cv2.COLOR_BGR2GRAY)[xyTemplate.y:(xyTemplate.y + xyTemplate.h), xyTemplate.x:(xyTemplate.x + xyTemplate.w)] 


        # Save images
        cv2.imwrite('../src/images/frame{}.png'.format(i), image_template)
        print('Captured!')
        i +=1    

